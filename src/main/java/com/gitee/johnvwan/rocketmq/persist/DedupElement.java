package com.gitee.johnvwan.rocketmq.persist;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class DedupElement {
    private String application;
    private String topic;
    private String tag;
    private String msgUniqKey;

}
